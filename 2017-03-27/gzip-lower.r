################################################################################
##
## Take a list of cds from genomic, keep acgt, convert to ACGT, determine
## uncompressed and gzipped size
##
## Run with:
##   Rscript --vanilla gzip-lower.r
##
################################################################################

options(width=160)
rm(list=ls())


################################################################################
## Settings

## date of the run
run.date = "2017-03-27"

## project directory
rootdir = "/home/paul/data/18.projects/03.dna-entropy"

## directory for this analysis
crtdir = paste0(rootdir,"/",run.date)

## file name containing list of genomes
genome.list = paste0(rootdir,"/download-refseq/genomes-list.2017-03-24.RData")

## directory containing downloaded sequences
downdir = paste0(rootdir,"/download-refseq/refseq-genomes.2017-03-24")

## work directory for temporary files
wdir = paste0(crtdir,"/data-work")

## output results file
fout.r = paste0(crtdir,"/gzip-lower.RData")
fout.tab = paste0(crtdir,"/gzip-lower.tab")

## output log file
flog = paste0(crtdir,"/gzip-lower.log")


################################################################################


setwd(rootdir)

## make work directory
system(paste("mkdir -p", wdir))

## load genome list
load(genome.list)

## prepare output data
d.out = genomes.ref.rep
d.out$nucl.tot = as.numeric(NA)
d.out$nucl.acgt = as.numeric(NA)
d.out$gzip = as.numeric(NA)

## loop over genome list
for (i in 1:nrow(d.out)) {
    ftp = d.out$ftp_path[i]
    id = sub(".*/","",ftp)
    fseq = paste0(downdir,"/",id,"_genomic.fna.gz")
    if (file.exists(fseq)) {
        fwraw  = paste0(wdir,"/",id,".lower.raw")
        fwtr   = paste0(wdir,"/",id,".lower.tr")

        cmd = paste0("zcat ",fseq," | ./c-code/fasta2raw > ",fwraw)
        system(cmd)
        cmd = paste0("cat ",fwraw," | wc -c")
        d.out$nucl.tot[i] = as.numeric(system(cmd, intern=TRUE))

        cmd = paste0("cat ",fwraw," | ./c-code/convert-keep-lower > ",fwtr)
        system(cmd)
        cmd = paste0("cat ",fwtr," | wc -c")
        d.out$nucl.acgt[i] = as.numeric(system(cmd, intern=TRUE))

        cmd = paste0("cat ",fwtr," | gzip -c - | wc -c")
        d.out$gzip[i] = as.numeric(system(cmd, intern=TRUE))

        ## cleanup
        cmd = paste("rm",fwraw,fwtr)
        system(cmd)
    }
}

## output results
save(run.date,d.out,file=fout.r)
write.table(d.out,file=fout.tab,sep="\t", row.names=FALSE)
