################################################################################
##
## Compile results of gzipping whole genomes, cds, repeatmasker and genome -
## retepatmasker sequences. Genomes are from 2017-03-04 RefSeq list
## download-refseq/genomes-list.2017-03-24.RData. Data files are from
## directories 2017-03-25 and 2017-03-27
##


## initialize
options(width=160)
rm(list=ls())
setwd("/home/paul/data/18.projects/03.dna-entropy")

## read data from gzip runs
data.desc = matrix(c(
    "wg",       "2017-03-25/gzip-whole-genome.RData",
    "wg.r.1",   "2017-03-25/gzip-rand-whole-genome.1.RData",
    "wg.r.2",   "2017-03-25/gzip-rand-whole-genome.2.RData",
    "wg.r.3",   "2017-03-25/gzip-rand-whole-genome.3.RData",
    "wg.r.4",   "2017-03-25/gzip-rand-whole-genome.4.RData",
    "wg.r.5",   "2017-03-25/gzip-rand-whole-genome.5.RData",
    "cds",      "2017-03-25/gzip-cds.RData",
    "cds.r.1",  "2017-03-25/gzip-rand-cds.1.RData",
    "cds.r.2",  "2017-03-25/gzip-rand-cds.2.RData",
    "cds.r.3",  "2017-03-25/gzip-rand-cds.3.RData",
    "cds.r.4",  "2017-03-25/gzip-rand-cds.4.RData",
    "cds.r.5",  "2017-03-25/gzip-rand-cds.5.RData",
    "low",      "2017-03-27/gzip-lower.RData",
    "low.r.1",  "2017-03-27/gzip-rand-lower.1.RData",
    "low.r.2",  "2017-03-27/gzip-rand-lower.2.RData",
    "low.r.3",  "2017-03-27/gzip-rand-lower.3.RData",
    "low.r.4",  "2017-03-27/gzip-rand-lower.4.RData",
    "low.r.5",  "2017-03-27/gzip-rand-lower.5.RData",
    "up",       "2017-03-27/gzip-upper.RData",
    "up.r.1",   "2017-03-27/gzip-rand-upper.1.RData",
    "up.r.2",   "2017-03-27/gzip-rand-upper.2.RData",
    "up.r.3",   "2017-03-27/gzip-rand-upper.3.RData",
    "up.r.4",   "2017-03-27/gzip-rand-upper.4.RData",
    "up.r.5",   "2017-03-27/gzip-rand-upper.5.RData"
), ncol=2, byrow=TRUE)

data.read = lapply(data.desc[,2], function(x) {
    load(x)
    d.out
})

names(data.read) = data.desc[,1]


## check that rows are in the same order for all data frames
accs = data.read[[1]]$assembly_accession
length(accs)
length(unique(accs))
## 2056

dif = sapply(data.read, function(x) {
    sum(accs != x$assembly_accession)
})
sum(dif)
## 0


## clean up data
data.szcol = lapply(names(data.read), function(x) {
    result = data.read[[x]][,22:24]
    names(result) = paste(x,names(result),sep=".")
    result
})
data = do.call(cbind, data.szcol)
data = cbind(data.read[[1]][,1:21], data)

drop.col = grepl("[0-9]\\.nucl",names(data))
data = data[,!drop.col]


################################################################################
## save data as tab-delimited text

write.table(data, file="gzip-data.2017-03-30.tab", quote=FALSE, sep="\t",
            row.names=FALSE)


################################################################################
## do some plotting


## genomic

sz.total = data$wg.nucl.tot
sz.actg = data$wg.nucl.acgt
sz.gz =  data$wg.gzip
sz.r.gz = data[,paste0("wg.r.",1:5,".gzip")]
sz.ratio = sz.gz/sz.r.gz
sz.ratio.mean = apply(sz.ratio,1,mean)
sz.ratio.sd = apply(sz.ratio,1,sd)

plot(sz.actg, sz.ratio.mean, col=2, pch=2, ylim=c(0,1.1))

## cds

sz.total = data$cds.nucl.tot
sz.actg = data$cds.nucl.acgt
sz.gz =  data$cds.gzip
sz.r.gz = data[,paste0("cds.r.",1:5,".gzip")]
sz.ratio = sz.gz/sz.r.gz
sz.ratio.mean = apply(sz.ratio,1,mean)
sz.ratio.sd = apply(sz.ratio,1,sd)

points(sz.actg, sz.ratio.mean, col=3, pch=3)

## lower

sz.total = data$low.nucl.tot
sz.actg = data$low.nucl.acgt
sz.gz =  data$low.gzip
sz.r.gz = data[,paste0("low.r.",1:5,".gzip")]
sz.ratio = sz.gz/sz.r.gz
sz.ratio.mean = apply(sz.ratio,1,mean)
sz.ratio.sd = apply(sz.ratio,1,sd)

points(sz.actg, sz.ratio.mean, col=4, pch=4)

## upper

sz.total = data$up.nucl.tot
sz.actg = data$up.nucl.acgt
sz.gz =  data$up.gzip
sz.r.gz = data[,paste0("up.r.",1:5,".gzip")]
sz.ratio = sz.gz/sz.r.gz
sz.ratio.mean = apply(sz.ratio,1,mean)
sz.ratio.sd = apply(sz.ratio,1,sd)

points(sz.actg, sz.ratio.mean, col=5, pch=5)
