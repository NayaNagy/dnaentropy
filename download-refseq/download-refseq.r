################################################################################
##
## Make a list of RefSeq genomes from file
## ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt
##
## Run as
##   Rscript --vanilla download-refseq.r
##
################################################################################


options(width=160)
rm(list=ls())
setwd("/home/paul/data/18.projects/03.dna-entropy/download-refseq")

sink("download.2017-03-24.log",append=TRUE)

writeLines(as.character(Sys.time()))

infile = "assembly_summary_refseq.2017-03-24.txt"
writeLines("\nInput file:")
writeLines(infile)
## Date of the run
run.date = "2017-03-24"
writeLines("\nRun date:")
writeLines(run.date)


## Read file

genomes = read.delim(infile, fill=FALSE, skip=1, colClasses="character")
names(genomes) = sub(".*\\.","",names(genomes))


## Some statistics about the file

writeLines("\nRefseq categories:")
table(genomes$refseq_category,useNA="always")
writeLines("\nAssembly levels:")
table(genomes$assembly_level,useNA="always")
writeLines("\nGenome rep:")
table(genomes$genome_rep,useNA="always")


## Make subset

sel.ref = genomes$refseq_category == "reference genome"
sel.rep = genomes$refseq_category == "representative genome"
sel.complete = genomes$assembly_level == "Complete Genome"
sel.chrom = genomes$assembly_level == "Chromosome"
sel.full = genomes$genome_rep == "Full"

genomes.ref = genomes[sel.ref & (sel.complete | sel.chrom) & sel.full,]
writeLines("\nReference genomes:")
writeLines(as.character(nrow(genomes.ref)))

genomes.rep = genomes[sel.rep & (sel.complete | sel.chrom) & sel.full,]
writeLines("\nRepresentative genomes:")
writeLines(as.character(nrow(genomes.rep)))

genomes.ref.rep = genomes[(sel.ref | sel.rep) & (sel.complete | sel.chrom)
                          & sel.full,]
writeLines("\nReference and representative genomes:")
writeLines(as.character(nrow(genomes.ref.rep)))


## save ref and ref/rep subsets
save(genomes.ref, genomes.ref.rep, file=paste0("genomes-list.",run.date,
                                               ".RData"))

write.table(genomes.ref, file=paste0("genomes-ref.",run.date,".tab"),
            sep="\t", row.names=FALSE, quote=FALSE)
write.table(genomes.ref.rep, file=paste0("genomes-ref-rep.",run.date,".tab"),
            sep="\t", row.names=FALSE, quote=FALSE)

## download files
outdir = paste0("refseq-genomes.",run.date)
system(paste0("mkdir ",outdir))

sapply(genomes.ref.rep$ftp_path, function(x) {
    org = gsub(".*/","",x)
    f.cds = paste0(org,"_cds_from_genomic.fna.gz")
    f.gen = paste0(org,"_genomic.fna.gz")
    
    response = system(paste0("( cd ", outdir, " && curl -O \"",
                             paste0(x, "/", f.cds), "\" )"), intern=TRUE)
    writeLines(response)
    Sys.sleep(2)
    response = system(paste0("( cd ", outdir, " && curl -O \"",
                             paste0(x, "/", f.gen), "\" )"), intern=TRUE)
    writeLines(response)
    Sys.sleep(2)
})

writeLines(as.character(Sys.time()))

sink()
