The file processing procedure was completely changed. It is now modular. Each
compiled program reads data from standard input and writes to standard
output. There are some driver scripts written in R that collect and summarize
size measurements.

Note that the maximum size of a file that can be shuffled is about 4 GB.

================================================================================
=== Sequences

Steps:

- read gzipped fasta files and output a concatenated, raw sequence string

    * fasta2raw
      gcc -O3 -o fasta2raw fasta2raw.c -lz

- read a raw sequence and do some transformation on it (e.g. discard anything
  that is not an A, C, G, or T; keep only lowercase (repeatmasker) a, c, g, t)

    * convert-upcase-base
      gcc -O3 -o convert-upcase-base convert-upcase-base.c transformations.c

    * convert-keep-lower
      gcc -O3 -o convert-keep-lower convert-keep-lower.c transformations.c
      
    * convert-keep-upper
      gcc -O3 -o convert-keep-upper convert-keep-upper.c transformations.c
      
- optionally shuffle the sequence

    * shuffle-seq
      gcc -O3 -o shuffle-seq shuffle-seq.c shuffle.c -lgsl -lcblas

- optionally compress with gzip

- read size in bytes with wc -c

Examples:

- Keep all A, C, G, Ts irrespective of case, convert them to uppercase, and
  discard other sequence characters. Read the total number of bytes of the
  uncompressed sequence

  cat GCF_000002545.3_ASM254v2_cds_from_genomic.fna.gz | fasta2raw | \
    convert-upcase-base | wc -c

- Keep only lowercase a, c, g, t (repeatmasker); convert them to uppercase;
  shuffle the sequence; compress it; read number of bytes of the compressed
  string.

  cat GCF_000002545.3_ASM254v2_genomic.fna.gz | fasta2raw | \
    convert-keep-lower | shuffle-seq | gzip -c - | wc -c

Driver R scripts:

- See for example 2017-03-27/gzip-rand-lower.r

================================================================================
== Binary computer files

It should be possible to use a similar approach and the same executables for
binary files. The approach would be as follows:

- Determine the byte count of the unchanged input file

  cat filename | wc -c

- Determine the byte count of the compressed input file

  cat filename | gzip -c - | wc -c

- Byte count of the shuffled, uncompressed file

  cat filename | shuffle-seq | wc -c

- Byte count of the shuffled, compressed file

  cat filename | shuffle-seq | gzip -c - | wc -c
