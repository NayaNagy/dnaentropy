options(width=160)
rm(list=ls())
setwd("/home/paul/data/18.projects/03.dna-entropy/2017-02-25")

res = read.delim("data-out/gzip-fasta-sizes.txt",header=FALSE, colClasses="character")

names(res) = c(
    "infile",
    "sz.total",
    "sz.actg",
    "sz.gz"
)

res$sz.total = as.numeric(res$sz.total)
res$sz.actg = as.numeric(res$sz.actg)
res$sz.gz = as.numeric(res$sz.gz)

res$ratio = res$sz.gz/res$sz.actg

res$is.cds = 0
res$is.cds[grepl("_cds_", res$infile)] = 1

svg("plot-both.svg")
plot(res$sz.actg, res$ratio, col=res$is.cds + 2, pch=res$is.cds + 2)
dev.off()
svg("plot-genome.svg")
plot(res$sz.actg[res$is.cds == 0], res$ratio[res$is.cds == 0], col=2, pch=2)
dev.off()
svg("plot-cds.svg")
plot(res$sz.actg[res$is.cds == 1], res$ratio[res$is.cds == 1], col=3, pch=3)
dev.off()
