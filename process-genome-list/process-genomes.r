################################################################################
##
## Take a list of genomes, download each from NCBI and send it throug the
## gzip-fasta program
##
################################################################################

options(width=160)
rm(list=ls())
setwd("/home/paul/data/18.projects/03.dna-entropy")


## Date of the run
run.date = "2017-03-03"


## Make directories
dir.i = paste0("./",run.date,"/data-in")
dir.w = paste0("./",run.date,"/data-work")
dir.o = paste0("./",run.date,"/data-out")
system(paste("mkdir -p", dir.i, dir.w, dir.o))


## Set name of file containing results
f.results = paste0(dir.o, "/gzip-fasta-sizes.txt")


## Read list of genomes to process (subset of
## assembly_summary_refseq.2017-02-25.txt)

load("genome-list/genomes-list.RData")


## Work on a small sample
genomes.test = genomes.ref[sample(1:nrow(genomes.ref), 5),]


## Do the work on this dataset
genomes.work = genomes.ref.rep


## Loop over all FTP URLs

for (ftp.root in genomes.work$ftp_path) {
    ## Download files from NCBI
    
    ## ftp.root = "ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/006/765/GCF_000006765.1_ASM676v1"
    
    g.id = sub(".*\\/","",ftp.root)
    fin.stats = paste0(g.id, "_assembly_stats.txt")
    fin.cds = paste0(g.id, "_cds_from_genomic.fna.gz")
    fin.genomic = paste0(g.id, "_genomic.fna.gz")
    
    ftp.str = paste0(ftp.root, "/{",fin.stats,",",fin.cds,",",fin.genomic,"}")
    
    curl.cmd = paste0("curl ", ftp.str, " --create-dirs -o ",dir.i,"/",g.id,"/#1")
###
    system(curl.cmd)
    
    
    ## Run our C program on the seuqence files
    
    system(paste0("mkdir ",dir.w,"/",g.id))
    
    ## codons
    arg.fin = paste0(dir.i,"/",g.id,"/",fin.cds)
    arg.fout = paste0(dir.w,"/",g.id,"/",g.id,"_cds_from_genomic")
    gzip.cmd = paste("./gzip-fasta ",arg.fin,arg.fout,">>",f.results)
    system(gzip.cmd)
    
    ##genomic
    arg.fin = paste0(dir.i,"/",g.id,"/",fin.genomic)
    arg.fout = paste0(dir.w,"/",g.id,"/",g.id,"_genomic")
    gzip.cmd = paste("./gzip-fasta ",arg.fin,arg.fout,">>",f.results)
    system(gzip.cmd)
}

