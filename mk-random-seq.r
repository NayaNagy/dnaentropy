library(seqinr)

setwd("/home/paul/data/18.projects/03.dna-entropy/data-random")

emin=3
emax=10
estep = 10

len = rep(10^(emin:(emax-1)),each=estep) * (10^(1/estep))^(0:(estep-1))
len = c(len,10^emax)
len=round(len)


set.seed(1484543848)

fmt = paste0("%0",(emax+1),"d")
for (n in len) {
    print(n)
    seq = sample(c("A","T","C","G"), n, repl=TRUE)
    nfmt = sprintf(fmt,n)
    fout = paste0("random_",nfmt,".fna")
    write.fasta(seq,nfmt,fout)
    system(paste0("gzip ", fout))
}
