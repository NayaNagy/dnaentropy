#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>

#include "shuffle.h"

#define ARR_SZ 11

void print_arr (size_t *array, size_t n) {
  if (n < 1)
    return;
  
  printf("(%lu", array[0]);
  for (size_t i = 1;  i < n; i++) {
    printf(", %lu", array[i]);
  }
  printf(")");
}

int main (int argc, char *argv[]) {

  printf("RAND_MAX: %u\n", RAND_MAX);
  printf("UINT_MAX: %u\n", UINT_MAX);
  
  size_t n=-1;
  printf("max size_t: %lu\n\n", n);


  // Print names of generators from gsl

  const gsl_rng_type **ti, **t0;
  gsl_rng * r;

  t0 = gsl_rng_types_setup ();

  printf ("Available generators:\n");

  for (ti = t0; *ti != 0; ti++)
    {
      printf ("%s", (*ti)->name);
      r = gsl_rng_alloc (*ti);
      printf (", %ld", gsl_rng_min(r));
      printf (", %ld\n", gsl_rng_max(r));
      gsl_rng_free(r);
    }

  // Shuffle an array using the Mersenne Twister

  r = gsl_rng_alloc (gsl_rng_mt19937);
  
  // initialize the randum number generator
  // with a fixed int
  //gsl_rng_set(r, 20170312);
  // with a different value every time
  time_t t;
  gsl_rng_set(r, (size_t) time(&t));

  // initialize the array
  size_t arr[ARR_SZ];
  for (int i = 0; i < ARR_SZ; i++) {
    arr[i] = i*i;
  }

  printf("\nArray before shuffle:\n");
  print_arr(arr, ARR_SZ);
  printf("\n");

  // shuffle the array
  shuffle(arr, ARR_SZ, r);

  printf("\nArray after shuffle:\n");
  print_arr(arr, ARR_SZ);
  printf("\n");

  
  
  gsl_rng_free (r);
  return 0;
}
