#include "transformations.h"


void upcase_base (char *seq, size_t *n) {
  size_t j = 0;
  
  for (size_t i = 0; i < *n; i++) {
    switch(seq[i]) {
    case 'a':
    case 'A':
      seq[j++] = 'A';
      break;
    case 't':
    case 'T':
      seq[j++] = 'T';
      break;
    case 'c':
    case 'C':
      seq[j++] = 'C';
      break;
    case 'g':
    case 'G':
      seq[j++] = 'G';
    };
  }

  *n = j;
}

void keep_lower (char *seq, size_t *n) {
  size_t j = 0;
  
  for (size_t i = 0; i < *n; i++) {
    switch(seq[i]) {
    case 'a':
      seq[j++] = 'A';
      break;
    case 't':
      seq[j++] = 'T';
      break;
    case 'c':
      seq[j++] = 'C';
      break;
    case 'g':
      seq[j++] = 'G';
    };
  }

  *n = j;
}

void keep_upper (char *seq, size_t *n) {
  size_t j = 0;
  
  for (size_t i = 0; i < *n; i++) {
    switch(seq[i]) {
    case 'A':
      seq[j++] = 'A';
      break;
    case 'T':
      seq[j++] = 'T';
      break;
    case 'C':
      seq[j++] = 'C';
      break;
    case 'G':
      seq[j++] = 'G';
    };
  }

  *n = j;
}

