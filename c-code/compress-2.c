#include <zlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdint.h>
#include <gsl/gsl_permutation.h>

#include "kseq/kseq.h"

/* for inlining gsl_permutation get */
#define HAVE_INLINE

uint8_t encode(char ch, const gsl_permutation *p) {
  switch(ch) {
  case 'a':
  case 'A': return gsl_permutation_get(p, 0);
  case 't':
  case 'T': return gsl_permutation_get(p, 1);
  case 'c':
  case 'C': return gsl_permutation_get(p, 2);
  case 'g':
  case 'G': return gsl_permutation_get(p, 3);
  default: return 4;
  };
}

void mk_file_name(char *name, char *root, int i) {
  sprintf(name, "%s_%02d", root, i);
}

#define BUFSIZE 1024
int write_encoded(int fd, char *seq, int seq_len, const gsl_permutation *p) {
  char buf[BUFSIZE];
  uint8_t code = 0, crt_code;
  size_t code_cnt = 0, buf_pos = 0;
  
  /* traverse sequence */
  for (size_t i= 0; i < seq_len; i++) {
    crt_code = encode(seq[i], p);
    if (crt_code >= 4)
      continue;

    code += crt_code;
    code_cnt++;
    if (code_cnt % 4 == 0) {
      buf[buf_pos++] = code;
      code = 0;

      if (buf_pos >= BUFSIZE) {
	if (BUFSIZE != write(fd, buf, BUFSIZE)) {
	  fprintf(stderr, "Error writing to output file\n");
	  exit(4);
	}
	buf_pos = 0;
      }
    }
    else { /* make room to encode next base */
      code = code << 2;
    }
  }

  /* deal with possible incomplete group at the end */
  if (code_cnt % 4 != 0) {
    code = code << (2*(3 - code_cnt%4));
    buf[buf_pos++] = code;
  };
  if (buf_pos != write(fd, buf, buf_pos)) {
    fprintf(stderr, "Error writing to output file\n");
    exit(4);
  };
  
  return code_cnt;
}

KSEQ_INIT(gzFile, gzread)

/* length of filename */
#define LARGEBUFSIZE 65536
int main(int argc, char *argv[])
{   
  gzFile fp;
  kseq_t *ks;
  int fdout; /* file descriptor */
  int perm_cnt;
  char fout[LARGEBUFSIZE];
  char cmd[LARGEBUFSIZE];
  char readbuf[LARGEBUFSIZE];
  FILE *pipe;
  int raw_size, gzipped_size;
  
  if (argc != 3) {	/* check for incorrect number of arguments */
    fprintf(stderr, "Usage: %s input_file output_file\n", argv[0]);
    exit(1);
  }

  /* initialize permutation */
  gsl_permutation * code_perm = gsl_permutation_alloc (4);
  gsl_permutation_init (code_perm);
  perm_cnt = 0;

  /* begin output of result line */
  printf("%s", argv[1]);

  /* loop over all code permutations */
  do {
  /* open input file and initialize ks structure */
    fp = gzopen(argv[1], "r");
    if (fp == Z_NULL) {
      fprintf(stderr, "Could not open file \"%s\" for input\n", argv[1]);
      exit(2);
    }
    ks = kseq_init(fp);

    /* open output_file (truncate if already exists, create if not) */
    mk_file_name(fout, argv[2], perm_cnt);
    if ((fdout = open(fout, O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1) {
      fprintf(stderr, "Error creating or opening %s\n", argv[2]);
      exit(3);
    }

    /* work on one sequence per iteration */
    while (kseq_read(ks) >= 0) {
      //printf("%s\t%ld\n", ks->name.s, ks->seq.l);
    
      write_encoded(fdout, ks->seq.s, ks->seq.l, code_perm);
    }

    /* increase permutation counter */
    perm_cnt++;

    /* destroy sequence, close files */
    kseq_destroy(ks);
    gzclose(fp);
    close(fdout);

    /* read in wordcount of output file */
    sprintf(cmd, "wc -c %s", fout);
    pipe = popen(cmd, "r");
    fgets(readbuf, sizeof(readbuf), pipe);
    pclose(pipe);
    sscanf(readbuf, "%d", &raw_size);

    /* gzip output file */
    sprintf(cmd, "gzip %s", fout);
    system(cmd);

    /* read in wordcount of gzipped file */
    sprintf(cmd, "wc -c %s.gz", fout);
    pipe = popen(cmd, "r");
    fgets(readbuf, sizeof(readbuf), pipe);
    pclose(pipe);
    sscanf(readbuf, "%d", &gzipped_size);

    /* output results for current permutation */
    if (perm_cnt == 1)
      printf("\t%d", raw_size);
    printf("\t%d", gzipped_size);
  }
  //while (0);
  while (gsl_permutation_next(code_perm) == GSL_SUCCESS);

  /* write end of line of results line */
  printf("\n");
  
  /* clean up permutation*/
  gsl_permutation_free (code_perm);
  return 0;
}
