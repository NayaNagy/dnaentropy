#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define BUFSIZE 1024

int main(int argc, char **argv) {

   int fd1, fd2;	/* file descriptors */
   int i, j, n, count;	
   char buf1[BUFSIZE];	/* buffers for read/write operations */
   char buf2[BUFSIZE];
   short code;

   if (argc != 3) {	/* check for incorrect number of arguments */
      fprintf(stderr, "Usage: %s input_file output_file\n", argv[0]);
      exit(1);
   }

   if ((fd1 = open(argv[1], O_RDONLY)) == -1) {	/* open input_file */
      fprintf(stderr, "File %s does not exist\n", argv[1]);
      exit(2);
   }

   /* open output_file (truncate if already exists, create if not) */
   if ((fd2 = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1) {
      fprintf(stderr, "Error creating or opening %s\n", argv[2]);
      exit(3);
   }

   count = 0;	/* number of bases read from input_file */
   code = 0;	/* ASCII code of character written into output_file */
   j = 0;	/* index in the output (write) buffer */

   while ((n = read(fd1, buf1, BUFSIZE)) > 0) 	/* read blocks of BUFSIZE bytes from input_file */
      for (i=1; i<=n; i++) {			/* loop through each byte in the current block */
         switch (buf1[i-1]) {
            case 'A' : break;
            case 'T' : code = code + 1;
                       break;
            case 'C' : code = code + 2;
                       break;
            case 'G' : code = code + 3;
         };
         if (buf1[i-1]=='A' || buf1[i-1]=='T' || buf1[i-1]=='C' || buf1[i-1]=='G') {
            count++;
            if (count%4 == 0) {	/* each group of 4 bases is encoded into one byte (character) */
                buf2[j++] = code;
                if (j == BUFSIZE) /* when output buffer is full, write it to output_file */
                   if (BUFSIZE != write(fd2, buf2, BUFSIZE)) {
                      fprintf(stderr, "Error writing into %s\n", argv[2]);
                      exit(4);
                   }
                   else j=0;
                code = 0;
            }
            else code = code<<2; /* make room to encode next base */
         }
      };

   if (count%4 != 0) {	/* deal with possible incomplete group at the end */
      code = code << (2*(3-count%4));
      buf2[j++] = code;
   };
   if (j != write(fd2, buf2, j)) {
      fprintf(stderr, "Error writing into %s\n", argv[2]);
      exit(4);
   };

   close(fd1);
   close(fd2);
   exit(0);
}
