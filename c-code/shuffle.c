#include "shuffle.h"

void shuffle(size_t *array, size_t n, gsl_rng *r)
{
    if (n > 1) {
        size_t i;
	for (i = 0; i < n - 1; i++) {
	  size_t j = i + gsl_rng_uniform_int(r, n - i); 
	  size_t t = array[j];
	  array[j] = array[i];
	  array[i] = t;
	}
    }
}

void shuffle_char(char *array, size_t n, gsl_rng *r)
{
    if (n > 1) {
        size_t i;
	for (i = 0; i < n - 1; i++) {
	  size_t j = i + gsl_rng_uniform_int(r, n - i); 
	  size_t t = array[j];
	  array[j] = array[i];
	  array[i] = t;
	}
    }
}
