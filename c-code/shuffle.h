/*******************************************************************************
 *
 * Implemetation of the Fisher-Yates shuffle algorithm to produce a random
 * permutation of the contents of an array
 *
 * See:
 *   https://xlinux.nist.gov/dads/HTML/fisherYatesShuffle.html
 *   http://benpfaff.org/writings/clc/shuffle.html
 *
 */

#ifndef __F_Y_SHUFFLE__
#define __F_Y_SHUFFLE__

#include <stdlib.h>
#include <gsl/gsl_rng.h>

/* Shuffle the n elements of array in random order using the random number
   generator r */
void shuffle(size_t *array, size_t n, gsl_rng *r);
void shuffle_char(char *array, size_t n, gsl_rng *r);

#endif /* __F_Y_SHUFFLE__ */
