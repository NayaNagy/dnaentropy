/*
 * Read sequences from a FASTA file and determine the total sequence length and
 * the size after applying gzip
 *
 * FASTA headers and newline characters are removed. All sequences are
 * concatenated into a single string.
 *
 * All of A, C, T, and G are converted to uppercase. Any other characters in the
 * sequence are skipped.
 *
 * The output contains of the following tab-separated words:
 *   - name of the input file
 *   - total length of original sequences, including characters other than ACTG
 *   - total length of output sequence, i.e. ACTG only
 *   - size of gzipped file in bytes
 *   - size of shuffled gzipped file in bytes
 */

#include <zlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include "kseq/kseq.h"


char upcase_base (char base) {
  switch(base) {
  case 'a':
  case 'A': return 'A';
  case 't':
  case 'T': return 'T';
  case 'c':
  case 'C': return 'C';
  case 'g':
  case 'G': return 'G';
  default: return '-';
  };
}

KSEQ_INIT(gzFile, gzread)

/* length of long names, such as filenames, commands */
#define LARGEBUFSIZE 65536
/* size of output buffer */
#define OBUFSZ 65536
int main(int argc, char *argv[])
{   
  gzFile fp;
  kseq_t *ks;
  int fdout; /* file descriptor */
  int fdShuffleOut; // file descriptor for shuffled file
  char fout[LARGEBUFSIZE];
  char *fShuffleOut = malloc(LARGEBUFSIZE);    //in real code you would check for errors in malloc here
  char cmd[LARGEBUFSIZE];
  char readbuf[LARGEBUFSIZE];
  FILE *pipe;
  uint64_t seq_len, raw_size, gzipped_size, shuffle_gzipped_size;
  char ch;

  /* vars for buffered output */
  char obuf[OBUFSZ];
  size_t buf_pos = 0;
  
  if (argc != 3) {	/* check for incorrect number of arguments */
    fprintf(stderr, "Usage: %s input_file output_file\n", argv[0]);
    exit(1);
  }

  /* begin output of result line */
  printf("%s", argv[1]);

  /* open input file and initialize ks structure */
  fp = gzopen(argv[1], "r");
  if (fp == Z_NULL) {
    fprintf(stderr, "Could not open file \"%s\" for input\n", argv[1]);
    exit(2);
  }
  ks = kseq_init(fp);

  /* open output_file (truncate if already exists, create if not) */
  strcpy(fout, argv[2]);
  if ((fdout = open(fout, O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1) {
    fprintf(stderr, "Error creating or opening %s\n", argv[2]);
    exit(3);
  }

  /* open shuffled_output_file (truncate if already exists, create if not) */
  fShuffleOut[0] = '\0';
  strcat(fShuffleOut, fout);
  strcat(fShuffleOut, "Shuffle");
  if ((fdShuffleOut = open(fShuffleOut, O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1) {
    fprintf(stderr, "Error creating or opening %s\n", fout);
    exit(3);
  }

  // start the random variable to shuffle the file content 
  time_t t;
  srand((unsigned) time(&t));

  // work on one sequence per iteration 
  seq_len = 0;
  while (kseq_read(ks) >= 0) {
    //printf("%s\t%ld\n", ks->name.s, ks->seq.l);
    
    seq_len += ks->seq.l;
    for (size_t i = 0; i < ks->seq.l; i++) {
      ch = upcase_base(ks->seq.s[i]);
      if (ch != '-') {
	obuf[buf_pos++] = ch;
	if (buf_pos >= OBUFSZ) {
	  if (OBUFSZ != write(fdout, obuf, OBUFSZ)) {
	    fprintf(stderr, "Error writing to output file\n");
	    exit(4);
	  }
	  
	  //write the buffer to the shuffled output file as well.
          for (int sw = 1; sw <= 3*OBUFSZ; sw++)
          { 
            int key1 = rand() % OBUFSZ; 
            int key2 = rand() % OBUFSZ; 
            char temp = obuf[key1];
            obuf[key1] = obuf[key2];
            obuf[key2] = temp;
          }
	  if (OBUFSZ != write(fdShuffleOut, obuf, OBUFSZ)) {
	    fprintf(stderr, "Error writing to shuffled output file\n");
	    exit(4);
	  }
	  
	  buf_pos = 0;
	}
      }
    }
  }

 // output remaining sequence in output buffer 
  if (buf_pos > 0) {
    if (buf_pos != write(fdout, obuf, buf_pos)) {
      fprintf(stderr, "Error writing to output file\n");
      exit(4);
    }
  }

  for (int sw = 1; sw <= 3*buf_pos; sw++)
  { 
    int key1 = rand() % buf_pos; 
    int key2 = rand() % buf_pos; 

    char temp = obuf[key1];
    obuf[key1] = obuf[key2];
    obuf[key2] = temp;
  }

  /* output remaining sequence in shuffled output buffer */
  if (buf_pos > 0) {
    if (buf_pos != write(fdShuffleOut, obuf, buf_pos)) {
      fprintf(stderr, "Error writing to output file\n");
      exit(4);
    }
  }

  /* destroy sequence, close files */
  kseq_destroy(ks);
  gzclose(fp);
  close(fdout);
  close(fdShuffleOut);

  // read in wordcount of output file 
  sprintf(cmd, "wc -c %s", fout);
  pipe = popen(cmd, "r");
  fgets(readbuf, sizeof(readbuf), pipe);
  pclose(pipe);
  sscanf(readbuf, "%" PRIu64, &raw_size);
  
  // gzip output file 
  sprintf(cmd, "gzip %s", fout);
  system(cmd);
  
  // gzip shuffled output file 
  sprintf(cmd, "gzip %s", fShuffleOut);
  system(cmd);
  
  // read in wordcount of gzipped file 
  sprintf(cmd, "wc -c %s.gz", fout);
  pipe = popen(cmd, "r");
  fgets(readbuf, sizeof(readbuf), pipe);
  pclose(pipe);
  sscanf(readbuf, "%" PRIu64, &gzipped_size);

  // read in wordcount of shuffled gzipped file 
  sprintf(cmd, "wc -c %s.gz", fShuffleOut);
  pipe = popen(cmd, "r");
  fgets(readbuf, sizeof(readbuf), pipe);
  pclose(pipe);
  sscanf(readbuf, "%" PRIu64, &shuffle_gzipped_size);

  // output sizes 
  printf("\t%" PRIu64, seq_len);
  printf("\t%" PRIu64, raw_size);
  printf("\t%" PRIu64, gzipped_size);
  printf("\t%" PRIu64, shuffle_gzipped_size);
  printf("\n");
  
  return 0;
}
