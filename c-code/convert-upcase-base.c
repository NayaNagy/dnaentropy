/*
 * Use this as a template for writing programs that read characters from
 * standard input, do some transformation, and write the result to standard
 * output. The transformation is specified by function transform
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>

#include "transformations.h"

#define TRANSFORM upcase_base
#define BUF_SIZE 1048576


int main(int argc, char *argv[]) {   
  char buffer[BUF_SIZE];
  size_t content_size = 0;
  ssize_t bytes_read;
  
  // array into which to load input
  char *content = NULL;

  // read from standard input and load into content array
  while ((bytes_read = read(STDIN_FILENO, buffer, BUF_SIZE)) > 0) {
    content_size += bytes_read;
    content = realloc(content, sizeof(char) * content_size);
    if(content == NULL) {
      fprintf(stderr, "Failed to reallocate content for %lu", content_size);
      exit(1);
    }
    for (size_t i = 0; i < bytes_read; i++) {
      content[content_size - bytes_read + i] = buffer[i];
    }
  }

  //fprintf(stderr, "content size: %lu\n", content_size);


  // do the transformation
  TRANSFORM(content, &content_size);


  // write transformed array to standard output
  size_t bytes_to_write;
  ssize_t bytes_written;

  size_t write_crt = 0;

  while (write_crt < content_size) {
    bytes_to_write = BUF_SIZE;
    if (write_crt + BUF_SIZE > content_size) {
      bytes_to_write = content_size - write_crt;
    }

    bytes_written = write(STDOUT_FILENO, content + write_crt, bytes_to_write);
    
    assert(bytes_to_write == bytes_written);
    write_crt += BUF_SIZE;
  }

  //fprintf(stderr, "bytes written: %ld\n", bytes_written);
  
  return 0;
}
