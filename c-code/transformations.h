/*******************************************************************************
 *
 * Several transformations of sequence strings
 *
 */

#ifndef __SEQ_TRANSFORM__
#define __SEQ_TRANSFORM__

#include <stdlib.h>

/* Keep only specific base letters and convert them to uppercase (A,C,G,T) */
void upcase_base (char *seq, size_t *n);

/* Keep only lowercase base letters (a,c,g,t) and convert them to uppercase */
void keep_lower (char *seq, size_t *n);

/* Keep only uppercase base letters (A,C,G,T) */
void keep_upper (char *seq, size_t *n);


#endif /* __SEQ_TRANSFORM__ */
