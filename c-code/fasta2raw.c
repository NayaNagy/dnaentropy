/*
 * Read sequences from standard input in FASTA format, text or gzipped. Strip
 * headers and newlines and output to standard output as a single string of
 * nucleotides.
 */

#include <stdio.h>
#include <zlib.h>
#include <stdint.h>
#include <inttypes.h>

#include "kseq/kseq.h"

KSEQ_INIT(gzFile, gzread)

int main(int argc, char *argv[])
{   
  gzFile fp;
  kseq_t *ks;
  uint64_t seq_len;

  fp = gzdopen(fileno(stdin), "r");
  ks = kseq_init(fp);
  while (kseq_read(ks) >= 0) {
    printf("%s",ks->seq.s);
  }

  return 0;
}
