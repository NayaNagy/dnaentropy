################################################################################
##
## Make a list of RefSeq genomes from file assembly_summary_refseq.txt
##
################################################################################


options(width=160)
rm(list=ls())
setwd("/home/paul/data/18.projects/03.dna-entropy/genome-list")

infile = "assembly_summary_refseq.2017-02-25.txt"


## Read file

genomes = read.delim(infile, fill=FALSE, skip=1, colClasses="character")
names(genomes) = sub(".*\\.","",names(genomes))


## Some statistics about the file

table(genomes$refseq_category,useNA="always")
##                na      reference genome representative genome                  <NA> 
##             81877                   164                  5749                     0 

table(genomes$assembly_level,useNA="always")

##   Chromosome Complete Genome          Contig        Scaffold            <NA> 
##         1146           13852           37353           35439               0 

table(genomes$genome_rep,useNA="always")

##  Full Partial    <NA> 
## 87789       1       0 


## Make subset

sel.ref = genomes$refseq_category == "reference genome"
sel.rep = genomes$refseq_category == "representative genome"
sel.complete = genomes$assembly_level == "Complete Genome"
sel.chrom = genomes$assembly_level == "Chromosome"
sel.full = genomes$genome_rep == "Full"

genomes.ref = genomes[sel.ref & (sel.complete | sel.chrom) & sel.full,]
## Lost "Zika virus", level is "Scaffold"
dim(genomes.ref)
## [1] 163  21

genomes.rep = genomes[sel.rep & (sel.complete | sel.chrom) & sel.full,]
dim(genomes.rep)
## [1] 1887   21

genomes.ref.rep = genomes[(sel.ref | sel.rep) & (sel.complete | sel.chrom) & sel.full,]
dim(genomes.ref.rep)
## [1] 2050   21

save(genomes.ref, genomes.ref.rep, file="genomes-list.RData")

write.table(genomes.ref, file="genomes-ref.tab", sep="\t", row.names=FALSE, quote=FALSE)
write.table(genomes.ref.rep, file="genomes-ref-rep.tab", sep="\t", row.names=FALSE,
            quote=FALSE)
