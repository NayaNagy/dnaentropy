################################################################################
##
## Results of gzipping executables, run by Naya


## initialize
options(width=160)
rm(list=ls())
setwd("/home/paul/data/18.projects/03.dna-entropy/2017-04-02")

## read data from excel
library(xlsx)
data.read = read.xlsx2("Executabile.xlsx", sheetIndex=1, colClasses="character",
                       stringsAsFactors=FALSE)



## clean up data
data = data.read[,1:8]
names(data) = c("name","initial.size","gzip",paste0("shuffle.gzip.",1:5))
lapply(2:ncol(data), function(i) {
    data[,i] <<- as.numeric(data[,i])
    return()
})


################################################################################
## plot executables

## all

sz.init = data$initial.size
sz.gz =  data$gzip
sz.r.gz = data[,paste0("shuffle.gzip.",1:5)]
sz.ratio = sz.gz/sz.r.gz
sz.ratio.mean = apply(sz.ratio,1,mean, na.rm=TRUE)
sz.ratio.sd = apply(sz.ratio,1,sd, na.rm=TRUE)

plot(sz.init, sz.ratio.mean, col=2, pch=2, ylim=c(0,1.1))

## exclude large outlier

sel = sz.init < max(sz.init)
plot(sz.init[sel], sz.ratio.mean[sel], col=2, pch=2, ylim=c(0,1.1))

