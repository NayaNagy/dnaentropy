################################################################################
##
## Take a list of cds from genomic, keep acgtACGT, convert to ACGT, shuffle,
## determine uncompressed and gzipped size
##
## Run with:
##   Rscript --vanilla gzip-rand-cds.r --args n
##
## n is a small integer indicating the run number
##
################################################################################

options(width=160)
rm(list=ls())


## read and validate command line
args = commandArgs(trailingOnly = TRUE)
ok = length(args) == 2 && args[1] == "--args" && !is.na(as.integer(args[2]))
if (!ok) {
    writeLines("Usage:\n  Rscript --vanilla gzip-rand-cds.r --args n")
    writeLines("n is a small integer denoting the run number")
    quit(save="no")
}

## save run number
run = as.integer(args[2])


################################################################################
## Settings

## date of the run
run.date = "2017-03-25"

## project directory
rootdir = "/home/paul/data/18.projects/03.dna-entropy"

## directory for this analysis
crtdir = paste0(rootdir,"/",run.date)

## file name containing list of genomes
genome.list = paste0(rootdir,"/download-refseq/genomes-list.2017-03-24.RData")

## directory containing downloaded sequences
downdir = paste0(rootdir,"/download-refseq/refseq-genomes.2017-03-24")

## work directory for temporary files
wdir = paste0(crtdir,"/data-work.",run)

## output results file
fout.r = paste0(crtdir,"/gzip-rand-cds.",run,".RData")
fout.tab = paste0(crtdir,"/gzip-rand-cds.",run,".tab")

## output log file
flog = paste0(crtdir,"/gzip-rand-cds.",run,".log")


################################################################################


setwd(rootdir)

## make work directory
system(paste("mkdir -p", wdir))

## load genome list
load(genome.list)

## prepare output data
d.out = genomes.ref.rep
d.out$nucl.tot = as.numeric(NA)
d.out$nucl.acgt = as.numeric(NA)
d.out$gzip = as.numeric(NA)

## loop over genome list
for (i in 1:nrow(d.out)) {
    ftp = d.out$ftp_path[i]
    id = sub(".*/","",ftp)
    fseq = paste0(downdir,"/",id,"_cds_from_genomic.fna.gz")
    if (file.exists(fseq)) {
        fwraw  = paste0(wdir,"/",id,".cds.raw")
        fwtr   = paste0(wdir,"/",id,".cds.tr")

        cmd = paste0("zcat ",fseq," | ./c-code/fasta2raw > ",fwraw)
        system(cmd)
        cmd = paste0("cat ",fwraw," | wc -c")
        d.out$nucl.tot[i] = as.numeric(system(cmd, intern=TRUE))

        cmd = paste0("cat ",fwraw," | ./c-code/convert-upcase-base",
                     " | ./c-code/shuffle-seq > ",fwtr)
        system(cmd)
        cmd = paste0("cat ",fwtr," | wc -c")
        d.out$nucl.acgt[i] = as.numeric(system(cmd, intern=TRUE))

        cmd = paste0("cat ",fwtr," | gzip -c - | wc -c")
        d.out$gzip[i] = as.numeric(system(cmd, intern=TRUE))

        ## cleanup
        cmd = paste("rm",fwraw,fwtr)
        system(cmd)
    }
}

## output results
save(run.date,d.out,file=fout.r)
write.table(d.out,file=fout.tab,sep="\t", row.names=FALSE)
